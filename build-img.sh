#!/bin/sh

image=$1
startdir=$2
registry=$3
project=$4

# Check whether the container is already there; terrible hack but docker search doesn't work
curl https://gitlab.com/$project/container_registry|grep "$image"
cret=$?

master_build=0
if test "$CI_PROJECT_NAMESPACE" = "phlerovium" && test "$CI_PROJECT_NAME" = "images";then
	master_build=1
fi

if test $master_build = 0 || test $cret = 0;then
	echo Image is already up-to-date
	exit 0
else
	set -e
	docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $registry
	docker build -t $registry/$project:$image $startdir
	docker push $registry/$project:$image
fi

exit 0
